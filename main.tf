# credentials

provider "aws" {
  region     = "us-east-2"
  access_key = "AKIA4AZ5KCUA3IJDXB6N"
  secret_key = "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"
}


# 1. Create vpc

resource "aws_vpc" "dresscode-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "dresscode"
  }
}

# 2. Create Internet Gateway

resource "aws_internet_gateway" "dresscode-gw" {
  vpc_id = aws_vpc.dresscode-vpc.id


}
# 3. Create Custom Route Table

resource "aws_route_table" "dresscode-route-table" {
  vpc_id = aws_vpc.dresscode-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.dresscode-gw.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.dresscode-gw.id
  }

  tags = {
    Name = "dresscode"
  }
}

# 4. Create a Subnet 

resource "aws_subnet" "dresscode-subnet" {
  vpc_id            = aws_vpc.dresscode-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-2a"

  tags = {
    Name = "dresscode-subnet"
  }
}

# 5. Associate subnet with Route Table
resource "aws_route_table_association" "dresscode-rts" {
  subnet_id      = aws_subnet.dresscode-subnet.id
  route_table_id = aws_route_table.dresscode-route-table.id
}
# 6. Create Security Group to allow port 22,80,443
resource "aws_security_group" "dresscode_sg" {
  name        = "allow_dresscode_traffic"
  description = "Allow dresscode inbound traffic"
  vpc_id      = aws_vpc.dresscode-vpc.id

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

   ingress {
    description = "Laravel - 1"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    description = "Laravel - 2"
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    description = "Laravel - 3"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

    ingress {
    description = "MySQL"
    from_port   = 33869
    to_port     = 33869
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "dresscode"
  }
}

# 7. Create a network interface with an ip in the subnet that was created in step 4

resource "aws_network_interface" "dresscode-nic" {
  subnet_id       = aws_subnet.dresscode-subnet.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.dresscode_sg.id]

}
# 8. Assign an elastic IP to the network interface created in step 7

resource "aws_eip" "dresscode_ip" {
  vpc                       = true
  network_interface         = aws_network_interface.dresscode-nic.id
  associate_with_private_ip = "10.0.1.50"
  depends_on                = [aws_internet_gateway.dresscode-gw]
}


# 9. Create Ubuntu server 

resource "aws_instance" "web-server-instance" {
  ami               = "ami-00399ec92321828f5"
  instance_type     = "t2.micro"
  availability_zone = "us-east-2a"
  key_name          = "dresscode_key"

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.dresscode-nic.id
  }

  tags = {
    Name = "dresscode-server"
  }
}



